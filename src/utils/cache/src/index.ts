/*
 * @ModuleName: 
 * @Author: yuetchn@163.com
 * @LastEditTime: 2023-01-12 12:16:59
 */
import { MD5 } from "@/utils/func"

export const GenerateCacheKey = (key: string) => `__c_u_k__${ MD5(key) }`