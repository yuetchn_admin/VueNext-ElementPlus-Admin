/*
 * @ModuleName: Local Storage
 * @Author: yuetchn@163.com
 * @LastEditTime: 2023-01-12 12:14:58
 */

import { DeepClone } from "@/utils/func"
import { GenerateCacheKey } from "./index";

export const SetLocalStorageByObject = (key:string, val:object) => {
  key = GenerateCacheKey(key)
  const _val = DeepClone(val)
  localStorage.setItem(key, JSON.stringify(_val))
}

export const GetLocalStorageByObject = <T>(key: string): T | null => {
  key = GenerateCacheKey(key)
  let _val = localStorage.getItem(key)
  if (!_val) {
    return null
  }
  try {
    _val = JSON.parse(_val)
  } catch (err) {
    // eslint-disable-next-line
    console.error(new Error(`localStorage：the '${ key }' key doesn't exist.`))
    return null
  }
  return _val ? (_val as unknown) as T : null
}

export const SetLocalStorageByString = (key:string, val:string) => localStorage.setItem(GenerateCacheKey(key), val)
export const GetLocalStorageByString = (key: string) => localStorage.getItem(GenerateCacheKey(key)) 
export const RemoveLocalStorage = (key:string) => localStorage.removeItem(GenerateCacheKey(key))