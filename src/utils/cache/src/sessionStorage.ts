/*
 * @ModuleName: Session Storage
 * @Author: yuetchn@163.com
 * @LastEditTime: 2023-01-12 12:15:48
 */
import { DeepClone } from "@/utils/func"
import { GenerateCacheKey } from "./index";

export const SetSessionStorageByObject = (key: string, val: object) => {
  key = GenerateCacheKey(key)
  const _val = DeepClone(val)
  sessionStorage.setItem(key, JSON.stringify(_val))
}
  
export const GetSessionStorageByObject = <T>(key: string):T|null => {
  key = GenerateCacheKey(key)
  let _val = sessionStorage.getItem(key)
  if (!_val) {
    return null
  }
  try {
    _val = JSON.parse(_val)
  } catch (err) {
    // eslint-disable-next-line
    console.error(new Error(`sessionStorage：the '${ key }' key doesn't exist.`))
    return null
  }
  return _val ? (_val as unknown) as T : null
}

export const SetSessionStorageByString = (key:string, val:string) => sessionStorage.setItem(GenerateCacheKey(key), val)
export const GetSessionStorageByString = (key: string) => sessionStorage.getItem(GenerateCacheKey(key))
export const RemoveSessionStorage = (key:string) => sessionStorage.removeItem(GenerateCacheKey(key))