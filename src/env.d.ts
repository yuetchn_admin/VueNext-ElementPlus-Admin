/*
 * @ModuleName: Vite环境变量
 * @Author: yuetchn@163.com
 * @LastEditTime: 2023-01-11 13:47:38
 */
/// <reference types="vite/client" />

declare module "*.vue" {
  import { DefineComponent } from "vue";
  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
  const component: DefineComponent<{}, {}, any>;
  export default component;
}

// all js
declare module "*.js";

interface ImportMetaEnv {
  /** BASE服务器地址 */
  VITE_BASE_HOST: string;
  /** 标题 */
  VITE_APP_TITLE: string;
  /** 是否显示Logo标题 */
  VITE_SHOW_LOGO_TITLE: string;
  /** 路由最大缓存数量 */
  VITE_ROUTER_CACHE_MAX: number;
  /** 路由模式 */
  VITE_ROUTER_MODE: string;
  /** Http Request Time Out */
  VITE_HTTP_REQUEST_TIME_OUT: number;
  /** Base Public Path */
  VITE_BASE_PUBLIC_PATH: string;
  // 项目默认起始路由地址
  VITE_DEFAULT_START_ROUTER_PATH: string;
  // 项目默认路由白名单
  VITE_DEFAULT_ROUTER_WRITES: string[];
  /** 全局请求失败重试次数 */
  VITE_GLOBAL_REQUEST_FAILED_RETRY_COUNT: number;
  /** 全局请求失败发送重试请求间隔时间，单位（毫秒） */
  VITE_GLOBAL_REQUEST_FAILED_RETRY_INTERVAL_TIME: number;
}
