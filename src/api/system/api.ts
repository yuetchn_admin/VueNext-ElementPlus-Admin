/*
 * @ModuleName: Api Api
 * @Author: yuetchn@163.com
 * @LastEditTime: 2022-12-22 17:39:46
 */
import req from "@/utils/request";
import { IRequestPageInfo, IResponsePageInfo } from "@/types";

export const Api_API = "system/api";
export const Apis_API = "system/apis";

export const GetApis = (params: IRequestPageInfo) => req.Get<IResponsePageInfo<any>>(Apis_API, params);
export const GetApi = (id: number) => req.Get(Api_API, { id });
export const PostApi = (data: any) => req.Post(Api_API, data);
export const PutApi = (data: any) => req.Put(Api_API, data);
export const DeleteApi = (ids: number[]) => req.Delete(Api_API, { ids });
