/*
 * @ModuleName: Role Api
 * @Author: yuetchn@163.com
 * @LastEditTime: 2023-01-10 12:52:48
 */
import { IRequestPageInfo, IResponsePageInfo } from "@/types";

import req from "@/utils/request";

export const Role_API = "system/role";
export const Roles_API = "system/roles";
export const RoleMenu_API = "system/roleMenu";
export const RoleApi_API = "system/roleApi";

/**
 * 获取角色列表
 * @param params
 * @returns
 */
export const GetRoles = (params: IRequestPageInfo) => req.Get<IResponsePageInfo<any>>(Roles_API, params);
export const GetRole = (role_id: number) => req.Get(Role_API, { role_id });
export const PutRole = (data: any) => req.Put(Role_API, data);
export const PostRole = (data: any) => req.Post(Role_API, data);
export const DeleteRole = (ids: number[]) => req.Delete(Role_API, { ids });

export const GetRoleMenu = (role_id: number) => req.Get(RoleMenu_API, { role_id });
export const PostRoleMenu = (data: { role_id: number, menu_ids: number[] }) => req.Post(RoleMenu_API, data);

export const GetRoleApi = (role_id: number) => req.Get(RoleApi_API, { role_id });
export const PostRoleApi = (data: { role_id: number, api_ids: number[] }) => req.Post(RoleApi_API, data);
