/*
 * @ModuleName:
 * @Author: yuetchn@163.com
 * @LastEditTime: 2022-12-29 21:57:53
 */
import req from "@/utils/request";

export const Menu_API = "system/menu";
export const MenuTree_API = "system/menuTree";
export const MenuChildren_API = "system/menuChildren";
export const MenuAll_API = "system/menuAll";

export const GetMenuTree = (type?: number) => req.Get(MenuTree_API, { type });
export const GetMenu = (id: number) => req.Get(Menu_API, { id });
export const PostMenu = (data: any) => req.Post(Menu_API, data);
export const PutMenu = (data: any) => req.Put(Menu_API, data);
export const DeleteMenu = (ids: number[]) => req.Delete(Menu_API, { ids });

export const GetMenuChildren = (parent_id: number, type = 0) => req.Get(MenuChildren_API, { parent_id, type });

export const GetMenuAll = (type?: number) => req.Get(MenuAll_API, { type });