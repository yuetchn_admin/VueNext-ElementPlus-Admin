/*
 * @ModuleName: InitDb API
 * @Author: yuetchn@163.com
 * @LastEditTime: 2023-01-09 01:25:53
 */
import req from "@/utils/request";

/**
 * 初始化数据库
 * @returns 
 */
export const GetInitDb = () => req.Get("system/initDb");