/*
 * @ModuleName: Http Request
 * @Author: yuetchn@163.com
 * @LastEditTime: 2023-01-05 16:07:36
 */

// 可按照实际项目要求进行设定
export interface IRequestPageInfo< T extends Record<string, any> | undefined = {} > {
  page_size: number
  page_number: number
  keyword: string
  order_field: string
  order: "asc" | "desc"
  query: T
}

/** 分页ResponseBody */
export interface IResponsePageInfo < T extends Record<string, any> = {} > {
  page_size: number
  page_number: number
  total: number
  list: T[]
}