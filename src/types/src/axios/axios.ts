/*
 * @ModuleName: Axios Types
 * @Author: yuetchn@163.com
 * @LastEditTime: 2023-01-11 15:19:48
 */

/** Response Data */
export interface IAxiosResponseBody<T> {
  code: number;
  data: T;
  msg: string;
}
