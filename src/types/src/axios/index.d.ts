/*
 * @ModuleName: Http Request 
 * @Author: yuetchn@163.com
 * @LastEditTime: 2023-01-11 15:20:12
 */
import { AxiosRequestConfig as _AxiosRequestConfig, AxiosResponse } from "axios";
import { IAxiosResponseBody } from "./axios";

declare module "axios" {
  export interface AxiosInstance {
    // eslint-disable-next-line
    < D = any > (config: _AxiosRequestConfig): Promise < AxiosResponse < IAxiosResponseBody < D >>> ;
    // eslint-disable-next-line
    < D = any >(url: string, config ? : _AxiosRequestConfig): Promise < AxiosResponse < IAxiosResponseBody < D > >> ;
  }

  export interface AxiosRequestConfig {
    /** 是否需要加载框，默认：true */
    loading?: boolean;
    /** 失败重试次数，默认值由环境变量设置 */
    failedRetryCount?: number;
    /** 私有变量，用于记录重试请求时间戳 */
    __timestamp?: number;
  }
}
